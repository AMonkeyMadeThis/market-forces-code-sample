package com.marketAnalysis.main.ui.presenters
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	public class AbstractPresenter extends EventDispatcher
	{
		// ---------------------------------------------------------------------
		//
		// vars
		//
		// ---------------------------------------------------------------------
		
		// ---------------------------------------------------------------------
		// dispatcher
		// ---------------------------------------------------------------------
		
		/**
		 * MATE event dispatcher
		 */
		private var _dispatcher:IEventDispatcher;
		
		public function get dispatcher():IEventDispatcher { return _dispatcher; }
		
		/**
		 * [constructor] AbstractPresenter
		 */
		public function AbstractPresenter(dispatcher:IEventDispatcher=null)
		{
			_dispatcher = dispatcher;
			
			super(dispatcher);
		}
	}
}