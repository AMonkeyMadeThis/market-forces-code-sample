package com.marketAnalysis.main.business.commands
{
	import com.marketAnalysis.main.business.ICommand;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	public class AbstractCommand extends EventDispatcher implements ICommand
	{
		[Bindable]
		public var dispatcher:IEventDispatcher;
		
		public function AbstractCommand(dispatcher:IEventDispatcher=null)
		{
			super(dispatcher);
			
			this.dispatcher = dispatcher;
		}
		
		public function execute(payload:*=null):void
		{
			throw new Error( this + '.execute() should be overridden' );
		}
		
	}
}