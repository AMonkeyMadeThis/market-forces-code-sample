package com.marketAnalysis.main.business.commands
{
	import com.marketAnalysis.main.business.events.MeasurementEvent;
	import com.marketAnalysis.main.business.vo.MeasurementVO;
	
	import flash.events.IEventDispatcher;
	
	public class AddMeasurementCommand extends AbstractCommand
	{
		public function AddMeasurementCommand(dispatcher:IEventDispatcher=null)
		{
			super(dispatcher);
		}
		
		override public function execute(payload:*=null):void
		{
			var _measurement:MeasurementVO = new MeasurementVO();
			_measurement.price = 0;
			_measurement.supply = 0;
			_measurement.demand = 0;
			
			dispatcher.dispatchEvent
			(
				new MeasurementEvent
				(
					MeasurementEvent.EDIT_MEASUREMENT,
					_measurement
				)
			);
		}
	}
}