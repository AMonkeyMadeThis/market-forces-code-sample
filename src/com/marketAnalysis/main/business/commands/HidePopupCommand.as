package com.marketAnalysis.main.business.commands
{
	import com.marketAnalysis.main.business.events.PopupEvent;
	
	import flash.display.DisplayObject;
	import flash.events.IEventDispatcher;
	
	import mx.core.FlexGlobals;
	import mx.core.IFlexDisplayObject;
	import mx.managers.PopUpManager;
	
	/**
	 * HidePopupCommand
	 */
	public class HidePopupCommand extends AbstractCommand
	{
		/**
		 * HidePopupCommand
		 */
		public function HidePopupCommand(dispatcher:IEventDispatcher=null)
		{
			super( dispatcher );
		}
		
		override public function execute(payload:*=null):void
		{
			var _payload:PopupEvent = payload as PopupEvent;
			
			if (_payload)
			{
				var _window:IFlexDisplayObject = _payload.window;
				
				PopUpManager.removePopUp( _window );
			}
		}
	}
}