package com.marketAnalysis.main.business.commands
{
	import com.marketAnalysis.dialogue.ui.views.Dialogue;
	import com.marketAnalysis.main.business.events.PopupEvent;
	
	import flash.events.IEventDispatcher;
	
	import mx.core.IFlexDisplayObject;
	
	public class ShowDialogueCommand extends AbstractCommand
	{
		public function ShowDialogueCommand(dispatcher:IEventDispatcher=null)
		{
			super(dispatcher);
		}
		
		override public function execute(payload:*=null):void
		{
			var _window:IFlexDisplayObject = new Dialogue();
			
			dispatcher.dispatchEvent
			(
				new PopupEvent
				(
					PopupEvent.SHOW_POPUP,
					_window
				)
			);
		}
	}
}