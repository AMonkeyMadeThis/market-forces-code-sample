package com.marketAnalysis.main.business.commands
{
	import com.marketAnalysis.main.business.events.PopupEvent;
	
	import flash.display.DisplayObject;
	import flash.events.IEventDispatcher;
	
	import mx.core.FlexGlobals;
	import mx.core.IFlexDisplayObject;
	import mx.managers.PopUpManager;
	
	/**
	 * ShowPopupCommand
	 */
	public class ShowPopupCommand extends AbstractCommand
	{
		/**
		 * ShowPopupCommand
		 */
		public function ShowPopupCommand(dispatcher:IEventDispatcher=null)
		{
			super( dispatcher );
		}
		
		override public function execute(payload:*=null):void
		{
			var _payload:PopupEvent = payload as PopupEvent;
			
			if (_payload)
			{
				var _window:IFlexDisplayObject = _payload.window;
				var _parent:DisplayObject = (_payload.parent!=null) ? _payload.parent : DisplayObject( FlexGlobals.topLevelApplication );
				var _modal:Boolean = _payload.modal;
				
				PopUpManager.addPopUp( _window, _parent, _modal );	
				PopUpManager.bringToFront( _window );
				PopUpManager.centerPopUp( _window );
			}
		}
	}
}