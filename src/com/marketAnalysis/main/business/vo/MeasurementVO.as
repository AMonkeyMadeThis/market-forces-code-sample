package com.marketAnalysis.main.business.vo
{
	/**
	 * MeasurementVO
	 */
	public class MeasurementVO
	{
		// ---------------------------------------------------------------------
		// price
		// ---------------------------------------------------------------------
		
		private var _price:Number;
		
		/**
		 * price
		 */
		public function get price():Number { return _price; }
		/**
		 * @private
		 */
		public function set price(value:Number):void
		{
			_price = value;
		}
		
		// ---------------------------------------------------------------------
		// supply
		// ---------------------------------------------------------------------
		
		private var _supply:int;
		
		/**
		 * supply
		 */
		public function get supply():int { return _supply; }
		/**
		 * @private
		 */
		public function set supply(value:int):void
		{
			_supply = value;
		}
		
		// ---------------------------------------------------------------------
		// demand
		// ---------------------------------------------------------------------
		
		private var _demand:int;
		
		/**
		 * demand
		 */
		public function get demand():int { return _demand; }
		/**
		 * @private
		 */
		public function set demand(value:int):void
		{
			_demand = value;
		}
		
		/**
		 * [constructor] MeasurementVO
		 */
		public function MeasurementVO()
		{
		}
	}
}