package com.marketAnalysis.main.business.services
{
	import com.marketAnalysis.main.business.events.MeasurementDataEvent;
	import com.marketAnalysis.main.business.vo.MeasurementVO;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	public class MeasurementDataService extends EventDispatcher
	{
		// ---------------------------------------------------------------------
		// dispatcher
		// ---------------------------------------------------------------------
		
		private var _dispatcher:IEventDispatcher;
		
		/**
		 * dispatcher
		 */
		public function get dispatcher():IEventDispatcher { return _dispatcher; }
		/**
		 * @private
		 */
		public function set dispatcher(value:IEventDispatcher):void
		{
			_dispatcher = value;
		}
		
		// ---------------------------------------------------------------------
		// data
		// ---------------------------------------------------------------------
		
		private var _data:ArrayCollection = new ArrayCollection();
		
		/**
		 * data
		 */
		public function get data():ArrayCollection { return _data; }
		/**
		 * @private
		 */
		public function set data(value:ArrayCollection):void
		{
			if (_data == value)
				return;
			_data = value;
		}
		
		public function MeasurementDataService(dispatcher:IEventDispatcher=null)
		{
			this.dispatcher = dispatcher;
			
			super(dispatcher);
		}
		
		public function addInitialData():void
		{
			update( buildMeasurement( 5.23, 107, 300 ) );
			update( buildMeasurement( 7.25, 150, 110 ) );
			update( buildMeasurement( 8.40, 260, 20 ) );
		}
		
		private function buildMeasurement(price:Number, supply:int, demand:int):MeasurementVO
		{
			var _measurement:MeasurementVO = new MeasurementVO();
			_measurement.price = price;
			_measurement.supply = supply;
			_measurement.demand = demand;
			
			return _measurement;
		}
		
		public function update(measurement:MeasurementVO):void
		{
			if (measurement && data.contains(measurement) == false)
			{
				data.addItem( measurement );
			}
			
			dispatchUpdatedData();
		}
		
		private function dispatchUpdatedData():void
		{
			dispatcher.dispatchEvent
			(
				new MeasurementDataEvent
				(
					MeasurementDataEvent.MEASUREMENT_DATA_CHANGED,
					data
				)
			);
		}
	}
}