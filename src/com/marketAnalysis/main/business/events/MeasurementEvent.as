package com.marketAnalysis.main.business.events
{
	import com.marketAnalysis.main.business.vo.MeasurementVO;
	
	import flash.events.Event;
	
	public class MeasurementEvent extends Event
	{
		public static const ADD_MEASUREMENT:String = 'addMeasurement';
		public static const EDIT_MEASUREMENT:String = 'editMeasurement';
		public static const SAVE_MEASUREMENT:String = 'saveMeasurement';
		public static const DELETE_MEASUREMENT:String = 'deleteMeasurement';
		
		// ---------------------------------------------------------------------
		// measurement
		// ---------------------------------------------------------------------
		
		private var _measurement:MeasurementVO;
		
		/**
		 * measurement
		 */
		public function get measurement():MeasurementVO { return _measurement; }
		/**
		 * @private
		 */
		public function updateMeasurement(value:MeasurementVO):void
		{
			_measurement = value;
		}
		
		/**
		 * [constructor]
		 */
		public function MeasurementEvent(type:String, measurement:MeasurementVO=null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			updateMeasurement( measurement );
			
			super(type, bubbles, cancelable);
		}
	}
}