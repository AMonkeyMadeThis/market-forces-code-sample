package com.marketAnalysis.main.business.events
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import mx.core.IFlexDisplayObject;
	import mx.managers.PopUpManager;
	
	public class PopupEvent extends Event
	{
		public static const SHOW_POPUP:String = 'showPopup';
		public static const HIDE_POPUP:String = 'hidePopup';
		
		// ---------------------------------------------------------------------
		// window
		// ---------------------------------------------------------------------
		
		private var _window:IFlexDisplayObject;
		
		/**
		 * window
		 */
		public function get window():IFlexDisplayObject { return _window; }
		/**
		 * @private
		 */
		private function updateWindow(value:IFlexDisplayObject):void
		{
			_window = value;
		}
		
		// ---------------------------------------------------------------------
		// parent
		// ---------------------------------------------------------------------
		
		private var _parent:DisplayObject;
		
		/**
		 * parent
		 */
		public function get parent():DisplayObject { return _parent; }
		/**
		 * @private
		 */
		private function updateParent(value:DisplayObject):void
		{
			_parent = value;
		}
		
		// ---------------------------------------------------------------------
		// modal
		// ---------------------------------------------------------------------
		
		private var _modal:Boolean;
		
		/**
		 * modal
		 */
		public function get modal():Boolean { return _modal; }
		/**
		 * @private
		 */
		private function updateModal(value:Boolean):void
		{
			if (_modal == value)
				return;
			_modal = value;
		}
		
		/**
		 * [constructor] PopupEvent
		 */
		public function PopupEvent(type:String, window:IFlexDisplayObject, parent:DisplayObject=null, modal:Boolean=true, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			updateWindow( window );
			updateParent( parent );
			updateModal( modal );
			
			super(type, bubbles, cancelable);
		}
	}
}