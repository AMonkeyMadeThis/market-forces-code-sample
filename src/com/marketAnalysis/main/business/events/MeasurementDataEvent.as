package com.marketAnalysis.main.business.events
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	public class MeasurementDataEvent extends Event
	{
		public static const MEASUREMENT_DATA_CHANGED:String = 'measurementDataChanged';
		
		// ---------------------------------------------------------------------
		// data
		// ---------------------------------------------------------------------
		
		private var _data:ArrayCollection;
		
		/**
		 * data
		 */
		public function get data():ArrayCollection { return _data; }
		/**
		 * @private
		 */
		private function updateData(value:ArrayCollection):void
		{
			_data = value;
		}
		
		public function MeasurementDataEvent(type:String, data:ArrayCollection, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			updateData( data );
			super(type, bubbles, cancelable);
		}
	}
}