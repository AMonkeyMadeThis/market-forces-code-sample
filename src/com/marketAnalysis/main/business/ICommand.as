package com.marketAnalysis.main.business
{
	public interface ICommand
	{
		function execute(payload:*=null):void;
	}
}