package com.marketAnalysis.dialogue.ui.views
{
	import com.marketAnalysis.dialogue.ui.presenters.DialoguePresenter;
	
	import flash.events.MouseEvent;
	
	import spark.components.Group;
	import spark.components.TextInput;
	import spark.events.TextOperationEvent;
	
	public class DialogueBase extends Group
	{
		[Bindable]
		public var presenter:DialoguePresenter;
		
		public var price_ti:TextInput;
		public var supply_ti:TextInput;
		public var demand_ti:TextInput;
		
		public function DialogueBase()
		{
			super();
		}
		
		protected function price_ti_changeHandler(event:TextOperationEvent):void
		{
			var _value:Number = Number(price_ti.text);
			presenter.updatePrice( _value ); 
		}
		
		protected function supply_ti_changeHandler(event:TextOperationEvent):void
		{
			var _value:int = int(supply_ti.text);
			presenter.updateSupply( _value ); 
		}
		
		protected function demand_ti_changeHandler(event:TextOperationEvent):void
		{
			var _value:int = int(demand_ti.text);
			presenter.updateDemand( _value ); 
		}
		
		protected function ok_btn_clickHandler(event:MouseEvent):void
		{
			presenter.save( this );
		}
		
		protected function cancel_btn_clickHandler(event:MouseEvent):void
		{
			presenter.cancel( this );
		}
	}
}