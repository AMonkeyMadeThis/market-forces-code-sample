package com.marketAnalysis.dialogue.ui.presenters
{
	import com.marketAnalysis.main.business.events.MeasurementEvent;
	import com.marketAnalysis.main.business.events.PopupEvent;
	import com.marketAnalysis.main.business.vo.MeasurementVO;
	import com.marketAnalysis.main.ui.presenters.AbstractPresenter;
	
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	
	import mx.core.IFlexDisplayObject;
	
	[Event(name="priceChanged", type="flash.events.Event")]
	[Event(name="supplyChanged", type="flash.events.Event")]
	[Event(name="demandChanged", type="flash.events.Event")]
	
	public class DialoguePresenter extends AbstractPresenter
	{
		// ---------------------------------------------------------------------
		// price
		// ---------------------------------------------------------------------
		
		private var _price:Number;
		
		[Bindable(event="priceChanged")]
		/**
		 * price
		 */
		public function get price():Number { return _price; }
		/**
		 * @private
		 */
		public function updatePrice(value:Number):void
		{
			_price = value;
			dispatchEvent( new Event( "priceChanged" ) );
		}
		
		// ---------------------------------------------------------------------
		// supply
		// ---------------------------------------------------------------------
		
		private var _supply:int;
		
		[Bindable(event="supplyChanged")]
		/**
		 * supply
		 */
		public function get supply():int { return _supply; }
		/**
		 * @private
		 */
		public function updateSupply(value:int):void
		{
			_supply = value;
			dispatchEvent( new Event( "supplyChanged" ) );
		}
		
		// ---------------------------------------------------------------------
		// demand
		// ---------------------------------------------------------------------
		
		private var _demand:int;
		
		[Bindable(event="demandChanged")]
		/**
		 * demand
		 */
		public function get demand():int { return _demand; }
		/**
		 * @private
		 */
		public function updateDemand(value:int):void
		{
			_demand = value;
			dispatchEvent( new Event( "demandChanged" ) );
		}
		
		// ---------------------------------------------------------------------
		// data
		// ---------------------------------------------------------------------
		
		private var _data:MeasurementVO;
		
		/**
		 * data
		 */
		public function get data():MeasurementVO { return _data; }
		/**
		 * @private
		 */
		public function updateData(value:MeasurementVO):void
		{
			_data = value;
		}
		
		public function DialoguePresenter(dispatcher:IEventDispatcher=null)
		{
			super(dispatcher);
		}
		
		public function update(measurement:MeasurementVO):void
		{
			if (measurement)
			{
				// set presenter values
				updatePrice( measurement.price );
				updateSupply( measurement.supply );
				updateDemand( measurement.demand );
				
				// save reference to the measurement
				updateData( measurement );
			}
		}
		
		public function save(view:IFlexDisplayObject):void
		{
			data.price = price;
			data.supply = supply;
			data.demand = demand;
			
			dispatcher.dispatchEvent
			(
				new MeasurementEvent
				(
					MeasurementEvent.SAVE_MEASUREMENT,
					data
				)
			);
			
			close( view );
		}
		
		public function cancel(view:IFlexDisplayObject):void
		{
			close( view );
		}
		
		private function close(view:IFlexDisplayObject):void
		{
			dispatcher.dispatchEvent
			(
				new PopupEvent
				(
					PopupEvent.HIDE_POPUP,
					view
				)
			);
		}
	}
}