package com.marketAnalysis.panel.ui.views
{
	import com.marketAnalysis.panel.ui.presenters.PanelPresenter;
	
	import flash.events.MouseEvent;
	
	import spark.components.Group;
	
	public class PanelBase extends Group
	{
		/**
		 * presenter
		 */
		public var presenter:PanelPresenter;
		
		/**
		 * [constructor] PanelBase
		 */
		public function PanelBase()
		{
			super();
		}
		
		/**
		 * click handler for addMeasurement_btn
		 */
		protected function addMeasurement_btn_clickHandler(event:MouseEvent):void
		{
			presenter.addMeasurement();
		}
	}
}