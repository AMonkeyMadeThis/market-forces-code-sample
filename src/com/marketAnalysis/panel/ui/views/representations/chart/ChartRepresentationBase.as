package com.marketAnalysis.panel.ui.views.representations.chart
{
	import com.marketAnalysis.panel.ui.presenters.ChartRepresentationPresenter;
	
	import spark.components.NavigatorContent;
	
	public class ChartRepresentationBase extends NavigatorContent
	{
		[Bindable]
		public var presenter:ChartRepresentationPresenter;
		
		public function ChartRepresentationBase()
		{
			super();
		}
	}
}