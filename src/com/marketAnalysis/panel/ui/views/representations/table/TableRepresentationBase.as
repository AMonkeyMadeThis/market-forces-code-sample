package com.marketAnalysis.panel.ui.views.representations.table
{
	import com.marketAnalysis.main.business.vo.MeasurementVO;
	import com.marketAnalysis.panel.ui.presenters.TableRepresentationPresenter;
	
	import spark.components.DataGrid;
	import spark.components.NavigatorContent;
	import spark.events.GridSelectionEvent;
	
	public class TableRepresentationBase extends NavigatorContent
	{
		[Bindable]
		public var presenter:TableRepresentationPresenter;
		
		public var dataGrid:DataGrid;
		
		public function TableRepresentationBase()
		{
			super();
		}
		
		protected function dataGrid_selectionChangeHandler(event:GridSelectionEvent):void
		{
			var _item:MeasurementVO = dataGrid.selectedItem as MeasurementVO;
			
			if (_item)
			{
				presenter.edit( _item );
			}
		}
	}
}