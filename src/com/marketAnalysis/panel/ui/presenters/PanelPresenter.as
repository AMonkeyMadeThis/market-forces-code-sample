package com.marketAnalysis.panel.ui.presenters
{
	import com.marketAnalysis.main.business.events.MeasurementEvent;
	import com.marketAnalysis.main.ui.presenters.AbstractPresenter;
	
	import flash.events.IEventDispatcher;
	
	public class PanelPresenter extends AbstractPresenter
	{
		public function PanelPresenter(dispatcher:IEventDispatcher=null)
		{
			super(dispatcher);
		}
		
		/**
		 * opens the dialogue
		 */
		public function addMeasurement():void
		{
			dispatcher.dispatchEvent
			(
				new MeasurementEvent
				(
					MeasurementEvent.ADD_MEASUREMENT
				)
			);
		}
	}
}