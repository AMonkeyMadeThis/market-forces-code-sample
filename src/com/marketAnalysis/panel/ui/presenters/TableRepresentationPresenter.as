package com.marketAnalysis.panel.ui.presenters
{
	import com.marketAnalysis.main.business.events.MeasurementEvent;
	import com.marketAnalysis.main.business.vo.MeasurementVO;
	import com.marketAnalysis.main.ui.presenters.AbstractPresenter;
	
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	public class TableRepresentationPresenter extends AbstractPresenter
	{
		// ---------------------------------------------------------------------
		// data
		// ---------------------------------------------------------------------
		
		private var _data:ArrayCollection = new ArrayCollection();
		
		[Bindable(event="dataChanged")]
		/**
		 * data
		 */
		public function get data():ArrayCollection { return _data; }
		/**
		 * @private
		 */
		public function updateData(value:ArrayCollection):void
		{
			_data.removeAll();
			_data.addAll( value );
			dispatchEvent( new Event( "dataChanged" ) );
		}
		
		public function TableRepresentationPresenter(dispatcher:IEventDispatcher=null)
		{
			super(dispatcher);
		}
		
		public function edit(measurement:MeasurementVO):void
		{
			dispatcher.dispatchEvent
			(
				new MeasurementEvent
				(
					MeasurementEvent.EDIT_MEASUREMENT,
					measurement
				)
			);
		}
	}
}